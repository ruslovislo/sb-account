package kz.ruslovislo.sb.account.config;

import kz.ruslovislo.sb.account.dto.*;
import kz.ruslovislo.sb.account.model.Role;
import kz.ruslovislo.sb.account.model.RoleFunction;
import kz.ruslovislo.sb.account.model.User;
import kz.ruslovislo.sb.account.model.UserRole;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {


    @Bean
    public ModelMapper modelMapper(){
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldMatchingEnabled(true)
                .setSkipNullEnabled(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);


        {
            PropertyMap<User, SessionUserDto> record = new PropertyMap<User, SessionUserDto>() {
                @Override
                protected void configure() {
                    map().setId(source.getId());
                    map().setLogin(source.getLogin());
                    map().setName(source.getName());
                }
            };
            mapper.typeMap(User.class, SessionUserDto.class).addMappings(record);
        }

        {
            PropertyMap<User, UserSignUpDto> record = new PropertyMap<User, UserSignUpDto>() {
                @Override
                protected void configure() {
                    map().setId(source.getId());
                    map().setLogin(source.getLogin());
                    map().setName(source.getName());
                    map().setPassword(null);
                }
            };
            mapper.typeMap(User.class, UserSignUpDto.class).addMappings(record);
        }

        {
            PropertyMap<UserRole, UserRoleDto> record = new PropertyMap<UserRole, UserRoleDto>() {
                @Override
                protected void configure() {
                    map().setId(source.getId());
                    map().setUserId(source.getUser().getId());
                    map().setRoleId(source.getRole().getId());
                    map().setRoleName(source.getRole().getName());
                    map().setRoleDescription(source.getRole().getDescription());

                }
            };
            mapper.typeMap(UserRole.class, UserRoleDto.class).addMappings(record);
        }
        {
            PropertyMap<RoleFunction, RoleFunctionDto> record = new PropertyMap<RoleFunction, RoleFunctionDto>() {
                @Override
                protected void configure() {
                    map().setRoleId(source.getRole().getId());
                    map().setFunctionId(source.getFunction().getId());
                    map().setFunctionName(source.getFunction().getName());
                    map().setFunctionDescription(source.getFunction().getDescription());

                }
            };
            mapper.typeMap(RoleFunction.class, RoleFunctionDto.class).addMappings(record);
        }
        return mapper;
    }
}
