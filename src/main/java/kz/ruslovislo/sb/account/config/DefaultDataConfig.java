package kz.ruslovislo.sb.account.config;

import kz.ruslovislo.sb.account.model.User;
import kz.ruslovislo.sb.account.repository.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DefaultDataConfig implements ApplicationListener<ContextRefreshedEvent> {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public DefaultDataConfig(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if(userRepository.findByLogin("test@test.kz").isPresent()==false){
            User user = new User();
            user.setLogin("test@test.kz");
            user.setName("Test");
            user.setPassword(bCryptPasswordEncoder.encode("test"));
            userRepository.save(user);
        }
    }
}
