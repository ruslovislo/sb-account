package kz.ruslovislo.sb.account.config;

import kz.ruslovislo.sb.account.repository.UserRepository;
import kz.ruslovislo.sb.account.security.CORSFilterX;
import kz.ruslovislo.sb.account.security.JWTAuthorizationFilter;
import kz.ruslovislo.sb.account.security.JwtAuthenticationFilter;
import kz.ruslovislo.sb.account.service.UserDetailsServiceImpl;
import kz.ruslovislo.sb.account.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Value("${jwt.token.secret}")
    private String secret;

    @Value("${jwt.token.expired}")
    private long validityInMilliseconds;

    private UserDetailsServiceImpl userDetailsService;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private UserRepository userRepository;

    private UserService userService;

    private ModelMapper modelMapper;

    public SecurityConfig(UserDetailsServiceImpl userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository, ModelMapper modelMapper, UserService userService) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository=userRepository;
        this.modelMapper = modelMapper;
        this.userService=userService;
    }



    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/login","/logout").permitAll()
                .antMatchers(HttpMethod.POST,"/login","/sign-up").permitAll()
                .anyRequest().authenticated()
                .and().logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(new LogoutSuccessHandler() {
                    @Override
                    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
                        System.out.println("LOGOUT SUCCESS");
                    }
                })

                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID")
                .and()
                .addFilterBefore(new CORSFilterX(), ChannelProcessingFilter.class)
                .addFilter(new JwtAuthenticationFilter(authenticationManager(), secret, validityInMilliseconds))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), secret, validityInMilliseconds,modelMapper,userRepository, userService));





//        http.exceptionHandling()
//                .authenticationEntryPoint((request, response, e) ->
//                {
//                    response.setContentType("application/json;charset=UTF-8");
//                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//                    response.setHeader("Access-Control-Allow-Credentials","true");
//                    response.setHeader("Access-Control-Allow-Origin","*");
//                    response.getWriter().write(
//                            objectMapper.writeValueAsString(new ErrorResponce("403","access denied"))
//                    );
//                });
    }

}
