package kz.ruslovislo.sb.account.security;

/**
 * @author Ruslan Temirbulatov on 9/23/19
 * @project usermngmnt-api
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

import kz.ruslovislo.sb.account.dto.UserAuthDto;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter{


    private AuthenticationManager authenticationManager;

    private String secret;
    private  long validityInMilliseconds;


    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, String secret, long validityInMilliseconds) {
        this.authenticationManager = authenticationManager;
        this.secret=secret;
        this.validityInMilliseconds = validityInMilliseconds;
    }



    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
//        try {



            UserAuthDto creds = new ObjectMapper()
                    .readValue(req.getInputStream(), UserAuthDto.class);

            Authentication a = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getLogin(),
                            creds.getPassword(),
                            new ArrayList<>())
            );

            return a;
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        User user = (User)auth.getPrincipal();

        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + validityInMilliseconds))
                .sign(Algorithm.HMAC512(secret.getBytes()));
        res.addHeader("Access-Control-Expose-Headers", "Authorization");
        res.addHeader("Authorization", "Bearer " + token);
        res.setHeader("Content-Type", "application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();


        out.print("{\"token\":\""+"Bearer " + token+"\"}");

        out.flush();
    }


}
