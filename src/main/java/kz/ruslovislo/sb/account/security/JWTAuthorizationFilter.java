package kz.ruslovislo.sb.account.security;

/**
 * @author Ruslan Temirbulatov on 9/23/19
 * @project usermngmnt-api
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;


import kz.ruslovislo.sb.account.dto.SessionUserDto;
import kz.ruslovislo.sb.account.model.User;
import kz.ruslovislo.sb.account.model.UserRole;
import kz.ruslovislo.sb.account.repository.UserRepository;
import kz.ruslovislo.sb.account.repository.UserRoleRepository;
import kz.ruslovislo.sb.account.service.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter{

    private static Logger log = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    private AuthenticationManager authenticationManager;
    private String secret;

    private  long validityInMilliseconds;

    private ModelMapper modelMapper;
    private UserRepository userRepository;
    private UserService userService;


    public JWTAuthorizationFilter(AuthenticationManager authManager, String secret, long validityInMilliseconds, ModelMapper modelMapper, UserRepository userRepository, UserService userService) {
        super(authManager);
        this.userRepository=userRepository;
        this.modelMapper=modelMapper;
        this.authenticationManager = authenticationManager;
        this.secret=secret;
        this.validityInMilliseconds = validityInMilliseconds;
        this.userService=userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {



        if(req.getRequestURI().trim().endsWith("/after-logout")){
            chain.doFilter(req, res);
            return;
        }

//        String header = req.getHeader(SecurityConstants.HEADER_STRING);


        if(SecurityContextHolder.getContext().getAuthentication()==null) {
            UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
//        authentication.




        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {



        String token = request.getHeader("Authorization");

        if (token == null || !token.startsWith("Bearer"))
            return null;

        if (token != null) {



            // parse the token.
            String user = null;
            try {
                user = JWT.require(Algorithm.HMAC512(secret.getBytes()))
                        .build()
                        .verify(token.replace("Bearer ", ""))
                        .getSubject();
            }catch (Exception e){
                log.error("token error",e);
                return null;
            }
            if (user != null) {


                SessionUserDto sessionUserDto = SessionUserDto.fromSession(request.getSession());

                if(sessionUserDto==null){
                    Optional<User> uo = userRepository.findByLogin(user);
                    if(uo.isPresent())
                        sessionUserDto = modelMapper.map(uo.get(), SessionUserDto.class);

                    if(sessionUserDto!=null) {
                        sessionUserDto.toSession(request.getSession());
                    }else
                        return null;
                }

                try {
                    return new UsernamePasswordAuthenticationToken(user, null, getGrants(user));
                } catch (Exception e) {
                    log.error("Getting user GRANTS error",e);
                    return null;
                }
            }
            return null;
        }
        return null;
    }


    private Set<GrantedAuthority> getGrants(String user) throws Exception {

        List<String> grants = userService.userFunctions(user);


        Set<GrantedAuthority> list = new HashSet<>();

        for(final String grant:grants){
            list.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "ROLE_"+grant;
                }
            });

        }



        return list;
    }
}
