package kz.ruslovislo.sb.account.dto;

import lombok.Data;

import javax.servlet.http.HttpSession;

/**
 * пользователь залогиненный в сессии
 */
@Data
public class SessionUserDto {

    private Long id;
    private String login;
    private String name;

    public static SessionUserDto fromSession(HttpSession session){
        SessionUserDto user = (SessionUserDto)session.getAttribute("user");
        return user;
    }

    public void toSession(HttpSession session){
        session.setAttribute("user",this);
    }
}
