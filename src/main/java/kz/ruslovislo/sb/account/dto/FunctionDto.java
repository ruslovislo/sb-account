package kz.ruslovislo.sb.account.dto;

import lombok.Data;

@Data
public class FunctionDto extends AbstractDto{

    private String name;
    private String description;
}
