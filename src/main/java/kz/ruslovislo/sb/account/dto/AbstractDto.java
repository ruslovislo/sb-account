package kz.ruslovislo.sb.account.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class AbstractDto {

    public static interface New {}

    public static interface Exist {}


    @Null(groups = {New.class})
    @NotNull(groups = {Exist.class})
    private Long id;
}
