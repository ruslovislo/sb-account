package kz.ruslovislo.sb.account.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoleDto extends AbstractDto{

    @NotNull(groups = {New.class})
    private String name;

    @NotNull(groups = {New.class, Exist.class})
    private String description;
}
