package kz.ruslovislo.sb.account.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class UserRoleDto extends AbstractDto{

    @NotNull(groups = {New.class})
    private Long userId;

    @NotNull(groups = {New.class})
    private Long roleId;

    @Null(groups = {New.class, Exist.class})
    private String roleName;

    @Null(groups = {New.class, Exist.class})
    private String roleDescription;
}
