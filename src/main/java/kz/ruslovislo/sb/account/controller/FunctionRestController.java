package kz.ruslovislo.sb.account.controller;

import kz.ruslovislo.pojo.table.TableDataRequest;
import kz.ruslovislo.sb.account.dto.AbstractDto;
import kz.ruslovislo.sb.account.dto.RoleDto;
import kz.ruslovislo.sb.account.service.RoleService;
import kz.ruslovislo.security.MethodAllowedDescription;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Map;

@RestController
@RequestMapping("/function")
public class FunctionRestController {

    private RoleService roleService;

    public FunctionRestController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping("/data")
    @RolesAllowed({"FUNCTION_DATA"})
    @MethodAllowedDescription("Список всех функций")
    public ResponseEntity data(@RequestBody TableDataRequest request)throws Exception{
        return new ResponseEntity<Map>(TableDataRequest.pageToRestDatatable(request,roleService.functionsData(request))  , HttpStatus.OK);
    }

}
