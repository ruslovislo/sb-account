package kz.ruslovislo.sb.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SbAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbAccountApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        BCryptPasswordEncoder e = new BCryptPasswordEncoder();
        return e;
    }
}
