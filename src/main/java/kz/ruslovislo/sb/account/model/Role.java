package kz.ruslovislo.sb.account.model;

import kz.ruslovislo.jpa.IdEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="RV_ROLE")
public class Role extends IdEntity {

    @Getter
    @Setter
    @Column(nullable = false, unique = true)
    private String name;

    @Getter
    @Setter
    @Column(nullable = false, unique = true)
    private String description;
}
