package kz.ruslovislo.sb.account.model;

import kz.ruslovislo.jpa.IdEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "RV_FUNCTION")
public class Function extends IdEntity {

    @Getter
    @Setter
    @Column(unique = true, nullable = false, updatable = false)
    private String name;

    @Getter
    @Setter
    @Column(unique = true, nullable = false)
    private String description;
}
