package kz.ruslovislo.sb.account.model;

import kz.ruslovislo.jpa.IdEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="RV_USER_ROLE", uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id","role_id"})})
public class UserRole extends IdEntity {

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;
}
