package kz.ruslovislo.sb.account.component;

import kz.ruslovislo.sb.account.model.UserRole;
import kz.ruslovislo.sb.account.repository.UserRoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRoleReader {

    private ModelMapper modelMapper;

    private UserRoleRepository userRoleRepository;

    public UserRoleReader(ModelMapper modelMapper, UserRoleRepository userRoleRepository) {
        this.modelMapper = modelMapper;
        this.userRoleRepository = userRoleRepository;
    }

    public UserRole oneOrNull(Long id){
        Optional<UserRole> userRole = userRoleRepository.findById(id);
        if(userRole.isPresent())
            return userRole.get();
        return null;
    }

    public UserRole oneOrException(Long id)throws Exception{
        UserRole userRole = oneOrNull(id);
        if(userRole==null)
            throw new Exception("Not found UserRole with id "+id);
        return userRole;
    }
}
