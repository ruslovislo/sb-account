package kz.ruslovislo.sb.account.repository;

import kz.ruslovislo.jpa.AbstractRepository;
import kz.ruslovislo.sb.account.model.Function;

import java.util.Optional;

public interface FunctionRepository extends AbstractRepository<Function> {

    public Optional<Function> findByName(String name);
}
