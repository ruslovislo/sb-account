package kz.ruslovislo.sb.account.repository;

import kz.ruslovislo.jpa.AbstractRepository;
import kz.ruslovislo.sb.account.model.User;

import java.util.Optional;

public interface UserRepository extends AbstractRepository<User> {

    public Optional<User> findByLogin(String login);
}
