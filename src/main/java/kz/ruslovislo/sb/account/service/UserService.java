package kz.ruslovislo.sb.account.service;

import kz.ruslovislo.pojo.table.TableDataRequest;
import kz.ruslovislo.sb.account.component.UserReader;
import kz.ruslovislo.sb.account.component.UserRoleReader;
import kz.ruslovislo.sb.account.dto.*;
import kz.ruslovislo.sb.account.model.Role;
import kz.ruslovislo.sb.account.model.RoleFunction;
import kz.ruslovislo.sb.account.model.User;
import kz.ruslovislo.sb.account.model.UserRole;
import kz.ruslovislo.sb.account.repository.RoleFunctionRepository;
import kz.ruslovislo.sb.account.repository.UserRepository;
import kz.ruslovislo.sb.account.repository.UserRoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    private UserRepository userRepository;

    private UserRoleRepository userRoleRepository;

    private ModelMapper modelMapper;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private RoleFunctionRepository roleFunctionRepository;

    private UserRoleReader userRoleReader;

    private UserReader userReader;
    public UserService(UserRepository userRepository, ModelMapper modelMapper, BCryptPasswordEncoder bCryptPasswordEncoder, UserRoleRepository userRoleRepository, UserRoleReader userRoleReader, RoleFunctionRepository roleFunctionRepository, UserReader userReader) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRoleRepository=userRoleRepository;
        this.userRoleReader=userRoleReader;
        this.roleFunctionRepository=roleFunctionRepository;
        this.userReader=userReader;
    }

    public Page<UserDto> userData(TableDataRequest request)throws Exception{
        Page<UserDto> page = TableDataRequest.findAll(UserDto.class, User.class, request, userRepository, new TableDataRequest.IBeanConverter<UserDto, User>() {
            @Override
            public UserDto convert(User entity) {
                return modelMapper.map(entity,UserDto.class);
            }
        });
        return page;
    }


    public UserSignUpDto signUp(UserSignUpDto signUpDto)throws Exception{
        User user = new User();
        user.setLogin(signUpDto.getLogin());
        user.setName(signUpDto.getName());
        user.setPassword(bCryptPasswordEncoder.encode(signUpDto.getPassword()));
        user = userRepository.save(user);

        return modelMapper.map(user, UserSignUpDto.class);

    }

    public boolean changePassword(Long userId, String oldPassword, String newPassword)throws Exception{
        User user = userReader.oneOrException(userId);
        if(user.getPassword()==null || newPassword==null || oldPassword==null || !bCryptPasswordEncoder.matches(oldPassword, user.getPassword())){
            return false;
        }

        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        userRepository.save(user);
        return true;
    }

    public UserRoleDto addRoleToUser(UserRoleDto userRoleDto)throws Exception{
        UserRole userRole = new UserRole();
        userRole.setUser(new User());
        userRole.getUser().setId(userRoleDto.getUserId());
        userRole.setRole(new Role());
        userRole.getRole().setId(userRoleDto.getRoleId());
        userRole = userRoleRepository.save(userRole);
        return modelMapper.map(userRole, UserRoleDto.class);
    }

    public void deleteUserRole(Long id)throws Exception{
        UserRole userRole = userRoleReader.oneOrException(id);
        userRoleRepository.delete(userRole);
    }


    public Page<UserRoleDto> userRoleData(TableDataRequest request)throws Exception{
        Page<UserRoleDto> page = TableDataRequest.findAll(UserRoleDto.class, UserRole.class, request, userRoleRepository, new TableDataRequest.IBeanConverter<UserRoleDto, UserRole>() {
            @Override
            public UserRoleDto convert(UserRole entity) {
                return modelMapper.map(entity,UserRoleDto.class);
            }
        });
        return page;
    }

    public List<String> userFunctions(String login)throws Exception{
        Set<String> set = new HashSet<>();
        List<UserRole> userRoles = userRoleRepository.findByUserLogin(login);
        if(userRoles!=null){
            for(UserRole userRole:userRoles){
                List<RoleFunction> roleFunctions = roleFunctionRepository.findByRoleId(userRole.getRole().getId());
                if(roleFunctions!=null)
                    for(RoleFunction roleFunction:roleFunctions)
                        set.add(roleFunction.getFunction().getName());
            }
        }
        List list = new LinkedList();
        list.addAll(set);
        return list;
    }

}
