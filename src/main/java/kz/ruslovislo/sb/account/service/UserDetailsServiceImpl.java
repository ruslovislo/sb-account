package kz.ruslovislo.sb.account.service;

import kz.ruslovislo.sb.account.model.User;
import kz.ruslovislo.sb.account.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        Optional<User> userOptional = userRepository.findByLogin(login);

        if (userOptional.isPresent() == false) {
            throw new UsernameNotFoundException(login);
        }

        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(userOptional.get().getLogin(), userOptional.get().getPassword(), new ArrayList<>() );

        return user;
    }
}
