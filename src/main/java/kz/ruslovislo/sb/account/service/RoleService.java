package kz.ruslovislo.sb.account.service;

import kz.ruslovislo.pojo.table.TableDataRequest;
import kz.ruslovislo.sb.account.component.FunctionReader;
import kz.ruslovislo.sb.account.component.RoleFunctionReader;
import kz.ruslovislo.sb.account.component.RoleReader;
import kz.ruslovislo.sb.account.dto.FunctionDto;
import kz.ruslovislo.sb.account.dto.RoleDto;
import kz.ruslovislo.sb.account.dto.RoleFunctionDto;
import kz.ruslovislo.sb.account.model.Function;
import kz.ruslovislo.sb.account.model.Role;
import kz.ruslovislo.sb.account.model.RoleFunction;
import kz.ruslovislo.sb.account.repository.FunctionRepository;
import kz.ruslovislo.sb.account.repository.RoleFunctionRepository;
import kz.ruslovislo.sb.account.repository.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {

    private FunctionRepository functionRepository;

    private RoleRepository roleRepository;

    private RoleFunctionRepository roleFunctionRepository;

    private ModelMapper modelMapper;

    private RoleReader roleReader;
    private FunctionReader functionReader;
    private RoleFunctionReader roleFunctionReader;


    public RoleService(FunctionRepository functionRepository, RoleRepository roleRepository, RoleFunctionRepository roleFunctionRepository, ModelMapper modelMapper, RoleReader roleReader, FunctionReader functionReader, RoleFunctionReader roleFunctionReader) {
        this.functionRepository = functionRepository;
        this.roleRepository = roleRepository;
        this.roleFunctionRepository = roleFunctionRepository;
        this.modelMapper = modelMapper;
        this.roleReader = roleReader;
        this.functionReader = functionReader;
        this.roleFunctionReader = roleFunctionReader;
    }

    private Role oneOrNull(Long id){
        Optional<Role> role = roleRepository.findById(id);
        if(role.isPresent())
            return role.get();
        return null;
    }

    public Role oneOrException(Long id)throws Exception{
        Role role = oneOrNull(id);
        if(role==null)
            throw new Exception("Not found Role with id "+id);
        return role;
    }

    public RoleDto create(RoleDto role) throws Exception{
        Role r = new Role();
        r.setName(role.getName());
        r.setDescription(role.getDescription());
        roleRepository.save(r);
        return modelMapper.map(r, RoleDto.class);
    }

    public Page<RoleDto> data(TableDataRequest request)throws Exception{
        Page<RoleDto> page = TableDataRequest.findAll(RoleDto.class, Role.class, request, roleRepository, new TableDataRequest.IBeanConverter<RoleDto, Role>() {
            @Override
            public RoleDto convert(Role role) {
                return modelMapper.map(role,RoleDto.class);
            }
        });
        return page;
    }

    public RoleDto update(RoleDto role) throws Exception{
        Role r = oneOrException(role.getId());
        r.setDescription(role.getDescription());
        roleRepository.save(r);
        return modelMapper.map(r, RoleDto.class);
    }

    public void delete(Long id) throws Exception{
        Role r = oneOrException(id);
        roleRepository.delete(r);
    }

    public Page<FunctionDto> functionsData(TableDataRequest request)throws Exception{
        Page<FunctionDto> page = TableDataRequest.findAll(FunctionDto.class, Function.class, request, functionRepository, new TableDataRequest.IBeanConverter<FunctionDto, Function>() {
            @Override
            public FunctionDto convert(Function function) {
                return modelMapper.map(function,FunctionDto.class);
            }
        });
        return page;
    }

    public RoleFunctionDto addFunctionToRole(RoleFunctionDto roleFunctionDto)throws Exception{
        RoleFunction roleFunction = new RoleFunction();
        roleFunction.setRole(roleReader.oneOrException(roleFunctionDto.getRoleId()));
        roleFunction.setFunction(functionReader.oneOrException(roleFunctionDto.getFunctionId()));
        roleFunction = roleFunctionRepository.save(roleFunction);
        return  modelMapper.map(roleFunction, RoleFunctionDto.class);
    }

    public void deleteRoleFunction(Long id)throws Exception{
        RoleFunction roleFunction = roleFunctionReader.oneOrException(id);
        roleFunctionRepository.delete(roleFunction);
    }

    public Page<RoleFunctionDto> roleFunctionData(TableDataRequest request)throws Exception{
        Page<RoleFunctionDto> page = TableDataRequest.findAll(RoleFunctionDto.class, RoleFunction.class, request, roleFunctionRepository, new TableDataRequest.IBeanConverter<RoleFunctionDto, RoleFunction>() {
            @Override
            public RoleFunctionDto convert(RoleFunction entity) {
                return modelMapper.map(entity,RoleFunctionDto.class);
            }
        });
        return page;
    }

}
